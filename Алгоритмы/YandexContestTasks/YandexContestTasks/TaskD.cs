﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Дано число n
/// Разбейте десятичную запись числа n на максимальное возможное количество различных чисел.
/// Использовать числа с незначащими нулями не разрешается.
/// Входной файл inputD.txt
/// Выходной файл outputD.txt
/// </summary>

namespace YandexContestTasks
{
    class TaskD
    {
        /// <summary>
        /// Чтение, решение, запись, консоль, диалог - вся программа 
        /// </summary>
        public static bool Run()
        {
            start:
            StreamReader fr;
            StreamWriter fw;
            string input_data;

            Console.WriteLine("Do you wish to generate the input.txt? \ny/any");
            var answer = Console.ReadKey();
            Console.WriteLine();

            /// Код генерации исходного файла
            /// Не участвует в подсчете времени выполнения задачи
            if (answer.KeyChar == 'y' || answer.KeyChar == 'н' || answer.KeyChar == 'Y' || answer.KeyChar == 'Н')
            {
                int n = 18;
                /*do
                {
                    Console.WriteLine("Put in the power of number n (0 < n < 19)");
                    n = Convert.ToInt32(Console.ReadLine());
                } while (n < 1 || n > 18);*/

                fw = new StreamWriter("inputD.txt");

                Random rand = new Random();

                input_data = "";
                for (int i = 0; i < n; i++)
                {
                    if (i == 0)
                    {
                        var digit = rand.Next(1, 10);
                        input_data += digit.ToString();
                    }
                    else
                    {
                        var digit = rand.Next(0, 10);
                        input_data += digit.ToString();
                    }
                }
                fw.WriteLine(input_data);
                fw.Close();
            }

            /// Начинаем отсчет времени после диалога с пользователем
            long time_start = System.DateTime.Now.Ticks;

            /// Читаем данные из файла
            fr = new StreamReader("inputD.txt");
            input_data = fr.ReadLine();
            fr.Close();

            /// Общаемся с пользователем
            Console.WriteLine("\nHere is generated value: {0}", input_data);
            Console.WriteLine("Process has been started. Please wait...\n", input_data);

            /// Открываем поток для записи
            fw = new StreamWriter("outputD.txt");
            fw.WriteLine("The value = {0}", input_data);

            /// Получение ответа
            List<string> ResultList5 = alg5(input_data);
            List<string> ResultList6 = alg6_forward(input_data);

            /// Записали ответ и закрыли поток
            bool result = WriteAnswer(ResultList5, fw, 5, input_data);
            fw.Close();

            /// Подсчитываем статистику работы программы
            long time_end = System.DateTime.Now.Ticks;
            long time_result = (time_end - time_start);
            /// Переводим время в мс
            time_result /= 10000;
            /// Выводим статистику
            var process = Process.GetCurrentProcess();
            Console.WriteLine("Done! The task has been completed for {0} milliseconds", time_result);
            Console.WriteLine("Maximum memory usage during the process is {0} Kb", (process.WorkingSet64 / 1024));



            Console.WriteLine("\nDo you wish to repeat the task D? y/any");
            if ((Console.ReadKey().KeyChar == 'y' ||
                Console.ReadKey().KeyChar == 'N' ||
                Console.ReadKey().KeyChar == 'т' ||
                Console.ReadKey().KeyChar == 'Т'))
            {
                Console.WriteLine();
                Console.Clear();
                goto start;
            }
            else
                return !result;
        }

        /// <summary>
        /// Вывод ответа в консоль и в файл
        /// </summary>
        /// <param name="digits"></param>
        /// <param name="fw"></param>
        /// <param name="alg_number"></param>
        /// <param name="str_to_check"></param>
        public static bool WriteAnswer(List<string> digits, StreamWriter fw, int alg_number, string str_to_check)
        {
            Console.Write("Here is result of alg #{0}: ", alg_number);
            fw.Write("Here is result of alg #{0}: ", alg_number);
            /// Выводим результаты
            for (int i = 0; i < digits.Count; i++)
            {
                fw.Write(digits[i]);
                Console.Write(digits[i]);
                if (i != digits.Count - 1)
                {
                    fw.Write("-");
                    Console.Write("-");
                }
            }
            fw.Write("\n");
            fw.WriteLine("Total count in this method = {0}", digits.Count);
            fw.WriteLine("Does the answer contain errors: {0}\n", CheckAnswer(str_to_check, digits));
            Console.Write("\n");
            Console.WriteLine("Total count in this method = {0}", digits.Count);
            Console.WriteLine("Does the answer contain errors: {0}\n", CheckAnswer(str_to_check, digits));
            return CheckAnswer(str_to_check, digits);
        }

        /// <summary>
        /// Проверка ответа по критериям:
        /// 1. Совпадение ответа с изначальной строкой
        /// 2. Отсутствие повторов
        /// 3. Отсутствие чисел, начинающихся с 0, но не являющихся одиночным нулём
        /// </summary>
        /// <param name="input_data"></param>
        /// <param name="result"></param>
        /// <returns> Вернет true, если есть хоть одна проблема; false во всех остальных случаях </returns>
        private static bool CheckAnswer(string input_data, List<string> result)
        {
            string res_str = "";
            for (int i = 0; i < result.Count; i++)
            {
                res_str += result[i];
            }

            /// Проверка #1
            if (!String.Equals(input_data, res_str))
                return true;

            /// Проверка #2
            for (int i = 0; i < result.Count; i++)
            {
                for (int j = 0; j < result.Count; j++)
                {
                    if (j != i)
                        if (result[i] == result[j])
                            return true;
                }
            }

            /// Проверка #3
            for (int i = 0; i < result.Count; i++)
                if (result[i].Length > 1 && result[i][0] == '0')
                    return true;
            return false;
        }

        /// <summary>
        /// Рабочий алгоритм
        /// </summary>
        /// <returns> Список полученных чисел </returns>
        private static List<string> alg5(string str)
        {
            List<Digit> digits = new List<Digit>();

            /// Создали массивчик
            for (int i = 0; i < str.Length; i++)
            {
                digits.Add(new Digit(str[i] + ""));
            }
            /// Посчитали у какого числа сколько повторов
            Recount(digits);

            /// Для нулей отдельная логика
            /// Разбираемся с нулями отдельно - объединим их в группы нулей
            SolveZeroProblem(digits);

            /// Теперь большой блок с рекурсией, который и выполняет основную работу - найти самое большое число разбиений
            /// Получим кандидатов на проверку
            List<int> candidates = getCandidates(digits);

            /// Все грустно, если кандидатов больше 2...
            /// Придется еще и соседей анализировать, причем делать это все в больших пределах O_O
            /// Все для того, чтобы сузить круг - уменьшить число кандидатов
            /// Зачем? Поскольку у нас будет рекурсия, надо как можно большее число беспонтовых вариантов отбросить
            /// Этим и займемся

            /// Найдем кандидатов с самым большим "весом"
            /// В качестве веса используется количество повторных вхождений числа в список всех чисел

            /// Ищем самый большой вес
            int max = 0;
            for (int i = 0; i < candidates.Count; i++)
                if (max < digits[candidates[i]].count) max = digits[candidates[i]].count;

            /// Нашли, теперь смотрим что нам это дает
            /// Если вес больше 5, то кандидатов точно > 10 - нас такая ситуация не устраивает
            /// Также нас не устраивает ситуация, когда у нас, например, 12 кандидатов, которые являются 6 разными числами с весом 2 у каждого - такое тоже надо отбросить
            /// Поэтому будем анализировать еще и соседей кандидата - за это отвечает переменная range_of_search
            if (max > 5 || candidates.Count > 10)
            {
                int range_of_search = 0;
                do
                {
                    range_of_search++;
                    candidates = getCandidates(digits, candidates, range_of_search);
                    if (range_of_search == 1 && candidates.Count > 4)
                    {
                        var a = candidates[0];
                        var b = candidates[candidates.Count - 1];
                        candidates.Clear();
                        candidates.Add(a);
                        candidates.Add(b);
                        break;
                    }
                } while (candidates.Count > 6 && range_of_search < digits.Count / 2 + 1);
            }

            /// А тут заводим рекурсию
            /// Смысл рекурсии - для всех кандидатов вызвать метод MergeAtIdx[candidates[i]] и для каждого следующего полученного списка продолжать выполнять алгоритм
            /// Выход из рекурсии, очевидно, когда candidates.Count == digits.Count && digits[every].count == 1 (второй проверкой убираем проблему, когда все цифры входят по n раз)
            int max_count = 0;
            List<List<Digit>> answers = new List<List<Digit>>();
            if (candidates.Count == digits.Count && digits[candidates[0]].count == 1)
                digits = digits;
            else
            {
                for (int i = 0; i < candidates.Count; i++)
                {
                    if (candidates[i] != digits.Count - 1)
                    {
                        SolveCandidatesProblem(answers, digits, candidates[i], ref max_count, 1);
                    }
                    if (candidates[i] != 0)
                    {
                        SolveCandidatesProblem(answers, digits, candidates[i], ref max_count, -1);
                    }
                }
                /// Выбираем лучший из них. Очевидно, лучший - с наибольшим числом различных чисел      
                digits = answers[0];
            }

            /// Закончили. Теперь дело за малым - вывести все на экран/в файл
            /// Поскольку вывод у нас требует в качестве параметра List<string>, его и "вырежем" из List<my_struct>
            List<string> to_return = new List<string>();
            for (int i = 0; i < digits.Count; i++)
                to_return.Add(digits[i].digit);

            return to_return;
        }

        /// <summary>
        /// Разбираемся с кандидатами на объединение
        /// </summary>
        /// <param name="answers"> Список со всеми вариантами разбиений </param>
        /// <param name="digits"> Текущий (начальный) список чисел </param>
        /// <param name="max"> Индекс кандидата, который будет объединен с соседом </param>
        private static void SolveCandidatesProblem(List<List<Digit>> answers, List<Digit> digits, int max, ref int max_count, int what = 0)
        {
            List<Digit> to_return = new List<Digit>();
            to_return.AddRange(digits);
            if (what == 0)
            {
                MergeAtIdx(to_return, max);
            }
            else
            {
                Merge(to_return, max, max + what);
            }
            var candidates = getCandidates(to_return);

            /// Проверим, а не ужасны ли наши дела
            max = 0;
            int range_of_search = 0;
            for (int i = 0; i < candidates.Count; i++)
            {
                if (max < to_return[candidates[i]].count) max = to_return[candidates[i]].count;
            }
            if ((max > 4 || candidates.Count > 8) && (max != 1))
            {
                do
                {
                    range_of_search++;
                    candidates = getCandidates(to_return, candidates, range_of_search);
                    if (range_of_search == 1 && candidates.Count > 4)
                    {

                        var a = candidates[0];
                        var b = candidates[candidates.Count - 1];
                        candidates.Clear();
                        candidates.Add(a);
                        candidates.Add(b);
                        break;
                    }
                } while (candidates.Count > 6 && range_of_search < to_return.Count / 2 + 1);
            }

            /// Тут конец рекурсии, если условие выполняется
            if (candidates.Count == to_return.Count && to_return[candidates[0]].count == 1)
            {
                if (to_return.Count > max_count)
                {
                    max_count = to_return.Count;
                    answers.Insert(0, to_return);
                }
            }
            else
            {
                for (int i = 0; i < candidates.Count; i++)
                {
                    SolveCandidatesProblem(answers, to_return, candidates[i], ref max_count);
                }
            }
            return;
        }

        /// <summary>
        /// Пересчет повторов для всех чисел списка
        /// </summary>
        /// <param name="l"> Обновляемый список </param>
        private static void Recount(List<Digit> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                int count = 0;
                for (int j = 0; j < l.Count; j++)
                {
                    if (l[i].digit == l[j].digit) count++;
                }
                l[i] = new Digit(count, l[i].digit);
            }
        }

        /*private static void Recount(List<Digit> digits, int merged_idx, string digit1, string digit2)
        {
            for (int i = 0; i < digits.Count; i++)
            {
                int modifier = 0;
                if (i == merged_idx) continue;
                if (digits[i].digit == digit1)
                    modifier--;
                if (digits[i].digit == digit2)
                    modifier--;
                if (modifier != 0)
                    digits[i].Count = modifier;
            }
        }*/

        /// <summary>
        /// Решаем проблемы с нулями, так как для них нужна особая логика
        /// </summary>
        /// <param name="digits"> Обновленный список, в котором невозможно получить числа, у которых впереди будет стоять ноль (если это не ноль сам по себе) </param>
        private static void SolveZeroProblem(List<Digit> digits)
        {
            string zeros = "";
            List<Digit> to_remove = new List<Digit>();
            for (int i = digits.Count - 1; i > 0; i--)
            {
                if (digits[i].digit == "0")
                {
                    zeros = "";
                    to_remove.Clear();
                    do
                    {
                        to_remove.Add(digits[i]);
                        if (to_remove.Count != 1)
                            zeros += "0";
                        i--;
                    } while (digits[i].digit == "0");

                    digits[i] = new Digit(digits[i].digit + zeros);
                    digits.RemoveRange(i + 1, zeros.Length);
                    Recount(digits);
                }
            }
            return;
        }

        /// <summary>
        /// Слияние чисел по заданному индексу 
        /// </summary>
        /// <param name="digits"> Список, в котором будем объединять числа </param>
        /// <param name="max"> Индекс, который надо объединить </param>
        private static void MergeAtIdx(List<Digit> digits, int max)
        {
            /// Алгоритм склеивания чисел
            Digit to_add = new Digit();
            string digit1 = "";
            string digit2 = "";
            /// Обработаем 2 случая
            /// Точно объединяем с левым
            if (max == digits.Count - 1)
            {
                if (digits[max - 1].digit == "0")
                {
                    digit1 = digits[max - 1].digit;
                    digit2 = digits[max].digit;
                    to_add = new Digit(digits[max - 1], digits[max]);
                    digits.RemoveAt(max);
                    max--;
                    digits.RemoveAt(max);
                    digits.Insert(max, to_add);
                    //Recount(digits, max, digit1, digit2);
                    digit1 = digits[max - 1].digit;
                    digit2 = digits[max].digit;
                    to_add = new Digit(digits[max - 1], digits[max]);
                    digits.RemoveAt(max);
                    max--;
                    digits.RemoveAt(max);
                    digits.Insert(max, to_add);
                }
                else
                {
                    digit1 = digits[max - 1].digit;
                    digit2 = digits[max].digit;
                    to_add = new Digit(digits[max - 1], digits[max]);
                    digits.RemoveAt(max);
                    max--;
                    digits.RemoveAt(max);
                    digits.Insert(max, to_add);
                }
                goto EndOfMerge;
            }
            /// Точно объединяем с правым
            if (max == 0)
            {
                digit1 = digits[max + 1].digit;
                digit2 = digits[max].digit;
                to_add = new Digit(digits[max], digits[max + 1]);
                digits.RemoveAt(max);
                digits.RemoveAt(max);
                digits.Insert(max, to_add);
                goto EndOfMerge;
            }
            /// Остальные случаи
            if (digits[max].digit == "0" || digits[max - 1].count > digits[max + 1].count && !(digits[max - 1].digit == "0"))
            {
                digit1 = digits[max - 1].digit;
                digit2 = digits[max].digit;
                to_add = new Digit(digits[max - 1], digits[max]);
                digits.RemoveAt(max);
                max--;
                digits.RemoveAt(max);
                digits.Insert(max, to_add);
                goto EndOfMerge;
            }
            if (digits[max - 1].digit == "0" || (digits[max - 1].count > digits[max + 1].count))
            {
                digit1 = digits[max + 1].digit;
                digit2 = digits[max].digit;
                to_add = new Digit(digits[max], digits[max + 1]);
                digits.RemoveAt(max);
                digits.RemoveAt(max);
                digits.Insert(max, to_add);
                goto EndOfMerge;
            }
            if (digits[max].digit != digits[max + 1].digit)
            {
                digit1 = digits[max + 1].digit;
                digit2 = digits[max].digit;
                to_add = new Digit(digits[max], digits[max + 1]);
                digits.RemoveAt(max);
                digits.RemoveAt(max);
                digits.Insert(max, to_add);
                goto EndOfMerge;
            }
            if (digits[max].digit != digits[max - 1].digit)
            {
                digit1 = digits[max - 1].digit;
                digit2 = digits[max].digit;
                to_add = new Digit(digits[max - 1], digits[max]);
                digits.RemoveAt(max);
                max--;
                digits.RemoveAt(max);
                digits.Insert(max, to_add);
                goto EndOfMerge;
            }

            digit1 = digits[max - 1].digit;
            digit2 = digits[max].digit;
            to_add = new Digit(digits[max - 1], digits[max]);
            digits.RemoveAt(max);
            max--;
            digits.RemoveAt(max);
            digits.Insert(max, to_add);
            goto EndOfMerge;

            /// Конец
            EndOfMerge:

            Recount(digits);
            return;

            //Recount(digits, max, digit1, digit2);
        }

        private static void Merge(List<Digit> digits, int idx_which, int idx_with)
        {
            Digit to_add = new Digit();
            if (idx_which - idx_with > 0)
            {
                if (digits[idx_with].digit != "0")
                {
                    to_add = new Digit(digits[idx_with], digits[idx_which]);
                    digits.RemoveRange(idx_with, 2);
                    digits.Insert(idx_with, to_add);
                }
                else
                {
                    string str = digits[idx_with - 1].digit + digits[idx_with].digit + digits[idx_which].digit;
                    to_add = new Digit(str);
                    digits.RemoveRange(idx_with-1, 3);
                    digits.Insert(idx_with - 1, to_add);
                }
            }
            else
            {
                if (digits[idx_which].digit != "0")
                {
                    to_add = new Digit(digits[idx_which], digits[idx_with]);
                    digits.RemoveRange(idx_which, 2);
                    digits.Insert(idx_which, to_add);
                }
                else
                {
                    string str = digits[idx_which - 1].digit + digits[idx_which].digit + digits[idx_with].digit;
                    to_add = new Digit(str);
                    digits.RemoveRange(idx_which-1, 3);
                    digits.Insert(idx_which - 1, to_add);
                }
            }
        }

        /// <summary>
        /// Получение кандидатов на применение к ним слияния
        /// </summary>
        /// <param name="digits"></param>
        /// <returns> Список, содержащий ИНДЕКСЫ кандидатов на слияние </returns>
        private static List<int> getCandidates(List<Digit> digits)
        {
            List<int> candidates = new List<int>();
            int max = 0;
            /// Подсчитываем метрику
            for (int i = 0; i < digits.Count; i++)
            {
                /// Думаем, что теперь с ней делать...
                /// Ну, найдем максимальную метрику (максимальное число повторов)
                if (digits[i].count > digits[max].count)
                {
                    max = i;
                }
            }
            /// Для начала запишем интересные циферки (индексы кандидатов), у которых число повторов == max 
            candidates.Clear();
            for (int i = 0; i < digits.Count; i++)
            {
                if (digits[i].count == digits[max].count)
                {
                    candidates.Add(i);
                }
            }
            return candidates;
        }

        /// <summary>
        /// Получение кандидатов среди кандидатов на применение к ним слияния
        /// </summary>
        /// <param name="digits"></param>
        /// <returns> Список, содержащий ИНДЕКСЫ кандидатов на слияние </returns>
        private static List<int> getCandidates(List<Digit> digits, List<int> candidates, int range_of_search)
        {
            if (candidates.Count == 1)
                return candidates;
            List<int> to_return = new List<int>();
            int max = 0;
            int max_count = 0, current_count = 0;
            /// Подсчитываем метрику
            for (int i = 1; i < candidates.Count; i++)
            {
                current_count = 0;
                max_count = 0;
                for (int j = -range_of_search; j <= range_of_search; j++)
                {
                    if (candidates[max] + j >= 0 && candidates[max] + j < digits.Count)
                    {
                        if (digits[candidates[max] + j].digit != digits[candidates[max]].digit)
                            max_count += digits[candidates[max] + j].count + 1;
                        else
                            max_count += digits[candidates[max] + j].count;
                    }
                    if (candidates[i] + j >= 0 && candidates[i] + j < digits.Count)
                    {
                        if (digits[candidates[i] + j].digit != digits[candidates[i]].digit)
                            current_count += digits[candidates[i] + j].count + 1;
                        else
                            current_count += digits[candidates[i] + j].count;
                    }
                }
                /// Думаем, что теперь с ней делать...
                /// Ну, найдем максимальную метрику (максимальное число повторов)
                if (current_count > max_count)
                {
                    max = i;
                }
            }
            /// Для начала запишем интересные циферки (индексы кандидатов), у которых число повторов == max 
            to_return.Clear();
            for (int i = 0; i < candidates.Count; i++)
            {
                current_count = 0;
                for (int j = -range_of_search; j <= range_of_search; j++)
                {
                    if (candidates[i] + j >= 0 && candidates[i] + j < digits.Count)
                    {
                        if (digits[candidates[i] + j].digit != digits[candidates[i]].digit)
                            current_count += digits[candidates[i] + j].count + 1;
                        else
                            current_count += digits[candidates[i] + j].count;
                    }
                }

                if (current_count == max_count)
                {
                    to_return.Add(candidates[i]);
                }
            }
            if (to_return.Count == candidates.Count)
            {
                to_return.RemoveRange(1, to_return.Count - 1);
            }
            return to_return;
        }


        private static List<string> alg6_forward(string str)
        {
            List<Digit> digits = new List<Digit>();

            /// Создали массивчик
            for (int i = 0; i < str.Length; i++)
            {
                digits.Add(new Digit(str[i] + ""));
            }
            /// Посчитали у какого числа сколько повторов
            Recount(digits);

            /// Для нулей отдельная логика
            /// Разбираемся с нулями отдельно - объединим их в группы нулей
            SolveZeroProblem(digits);

            for (int i = 0; i < digits.Count; i++)
            {
                if (digits[i].count > 1)
                {
                    MergeAtIdx(digits, i);
                    i--;
                }
            }

            /// Закончили. Теперь дело за малым - вывести все на экран/в файл
            /// Поскольку вывод у нас требует в качестве параметра List<string>, его и "вырежем" из List<my_struct>
            List<string> to_return = new List<string>();
            for (int i = 0; i < digits.Count; i++)
                to_return.Add(digits[i].digit);

            return to_return;
        }

    }

    /// <summary>
    /// Структура числа, входящего в строку чисел
    /// Имеет такие поля как:
    /// 1. digit - само число в string формате
    /// 2. count - число вхождений числа в массив/список
    /// 3. zero_string - флажок, указывающий на то, является ли число строкой нулей
    /// </summary>
    class Digit
    {
        public string digit;
        public int count = 1;

        public Digit()
        {

        }

        public Digit(string str)
        {
            count = 1;
            digit = str;
        }
        public Digit(int _count, string str)
        {
            count = _count;
            digit = str;
        }
        public Digit(Digit a, Digit b)
        {
            this.count = 1;
            this.digit = a.digit + b.digit;
        }
        public void ModifyCount(int modifier)
        {
            this.count = this.count + modifier;
        }
    }
}

