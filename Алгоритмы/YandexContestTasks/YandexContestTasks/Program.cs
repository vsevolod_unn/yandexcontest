﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace YandexContestTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            /// И так все ясно
            Console.Write("=======================================================");
            Console.Write(" Task D ");
            Console.Write("=======================================================\n");
            bool result;
            do
            {
                result = TaskD.Run();
            } while (false);

            Console.Write("\n");

            /// И так все ясно
            Console.Write("=======================================================");
            Console.Write(" Task E ");
            Console.Write("=======================================================\n");
            //TaskE.Run();

            Console.Write("\n");

            /// И так все ясно
            Console.Write("=======================================================");
            Console.Write(" Task F ");
            Console.Write("=======================================================\n");
            TaskF.Run();

            Console.Write("\n");
            Console.ReadKey();
        }
    }
}
