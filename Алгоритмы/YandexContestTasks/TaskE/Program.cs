﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskE
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("=======================================================");
            Console.Write(" Task E ");
            Console.Write("=======================================================\n");
            TaskE.Run();
        }
    }
}
