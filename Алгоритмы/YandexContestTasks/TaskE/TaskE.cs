﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskE
{
    class TaskE
    {
        /// <summary>
        /// Формирование исходного файла - в контесте не нужен
        /// </summary>
        public static void StartDialogue()
        {
            Console.WriteLine("Do you wish to generate the input.txt? \ny/any");
            var answer = Console.ReadKey();
            Console.WriteLine();

            /// Формирование исходного файла
            if (answer.KeyChar == 'y' || answer.KeyChar == 'н' || answer.KeyChar == 'Y' || answer.KeyChar == 'Н')
            {
                int n;
                int[,] input_data;
                StreamWriter fw = new StreamWriter("input.txt");

                Console.WriteLine("Введите число точек n (не менее 3)");
                n = Convert.ToInt32(Console.ReadLine());
                fw.WriteLine(n);

                input_data = new int[n, 3];
                Random rand = new Random();

                /// Формирование данных
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        input_data[i, j] = rand.Next(-10000, 10001);
                        fw.Write(input_data[i, j]);
                        Console.Write(input_data[i, j]);
                        if (j != 2)
                        {
                            fw.Write(" ");
                            Console.Write(" ");
                        }
                        else
                        {
                            fw.Write(" ");
                            fw.Write("\n");
                            Console.Write("\n");
                        }
                    }
                }
                fw.Close();
            }
            /// Чтение исходного файла

        }

        /// <summary>
        /// Запуск выполнения задания
        /// </summary>
        /// <returns></returns>
        public static bool Run()
        {
            start:
            StreamReader fr;
            int n;
            int[,] input_data;
            point3D[] points;

            StartDialogue();

            /// Чтение исходного файла
            fr = new StreamReader("input.txt");
            n = Convert.ToInt32(fr.ReadLine());
            input_data = new int[n, 3];
            points = new point3D[n];
            string value;
            char symbol;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    value = "";
                    do
                    {
                        symbol = (char)fr.Read();
                        value += symbol.ToString();
                    } while (symbol != ' ');
                    input_data[i, j] = Convert.ToInt32(value);
                    if (j % 3 == 2)
                    {
                        points[i] = new point3D(input_data[i, 0], input_data[i, 1], input_data[i, 2]);
                    }
                    if (j == 3)
                    {
                        fr.ReadLine();
                    }
                }
            }
            fr.Close();
            /// Конец чтения

            Console.WriteLine("Process has been started. Please wait...\n", input_data);

            /// TODO...
            int[] indexes = new int[n];
            for (int i = 0; i < n; i++)
            {
                indexes[i] = i;
            }

            bool result = false;

            int idx1 = 0;
            int idx2 = 0;
            int k;
            for (k = 1; k < indexes.Length; k++)
                for (idx1 = indexes.Length - 1; idx1 >= k; idx1--)
                {
                    if (k==indexes.Length-1)
                    {
                        int a = 1 + 2;
                    }
                    idx2 = idx1 - k;
                    Swapping(indexes, idx1, idx2);
                    result = Check(points, indexes);
                    if (!result) 
                        goto end;
                    Swapping(indexes, idx2, idx1);
                }
            end:
            MakeAnswer(indexes);

            Console.WriteLine("idx1: {0}, idx2: {1}\n", idx1+1, idx2+1);

            Console.WriteLine("\nDo you wish to repeat the task E? y/any");
            if (Console.ReadKey().KeyChar == 'y' ||
                Console.ReadKey().KeyChar == 'N' ||
                Console.ReadKey().KeyChar == 'т' ||
                Console.ReadKey().KeyChar == 'Т')
            {
                Console.WriteLine();
                Console.Clear();
                goto start;
            }
            else
            {
                return !result;
            }
        }

        /// <summary>
        /// Проверка на выпуклость в какой-то из плоскостей
        /// </summary>
        /// <param name="points"> Координаты точек в 3D </param>
        /// <returns> Да/нет </returns>
        public static bool Check(point3D[] points, int[] indexes)
        {
            vector3D v1, v2, v3;
            int N = points.Length;

            v1 = new vector3D(points[N - 2], points[N - 1]);
            v2 = new vector3D(points[N - 1], points[0]);
            v3 = v1 * v2;

            int sign1 = v3.e1 >= 0 ? 1 : -1;
            int sign2 = v3.e2 >= 0 ? 1 : -1;
            int sign3 = v3.e3 >= 0 ? 1 : -1;

            if (v3.e1 == 0 && v3.e2 == 0 && v3.e2 == 0) return false;

            for (int i = 0; i < N - 2; i++)
            {
                v1 = new vector3D(points[i], points[i + 1]);
                v2 = new vector3D(points[i + 1], points[i + 2]);
                v3 = v1 * v2;

                if (v3.e1 * sign1 <= 0 && v3.e2 * sign2 <= 0 && v3.e2 * sign3 <= 0) return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="indexes"></param>
        public static void MakeAnswer(int[] indexes)
        {
            StreamWriter fr = new StreamWriter("output.txt");
            foreach (int idx in indexes)
            {
                fr.Write("{0} ", idx + 1);
            }
            fr.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Swapping(int[] indexes, int idx1, int idx2)
        {
            int temp = indexes[idx1];
            indexes[idx1] = indexes[idx2];
            indexes[idx2] = temp;
        }

        /// <summary>
        /// Проекция на плоскость XoY
        /// </summary>
        /// <param name="data"> Массив исходных точек в 3D </param>
        /// <returns> Массив 2D точек (x, y) в проекции на выбранную плоскость </returns>
        public static int[,] XoY(int[,] data)
        {
            int n = data.GetLength(0);
            int[,] to_return = new int[data.GetLength(0), 2];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    switch (j)
                    {
                        case 0:
                            to_return[i, 0] = data[i, 0];
                            break;
                        case 1:
                            break;
                        case 2:
                            to_return[i, 1] = data[i, 1];
                            break;
                        default:
                            break;
                    }

                }
            }

            return to_return;
        }

        /// <summary>
        /// Проекция на плоскость ZoX
        /// </summary>
        /// <param name="data"> Массив исходных точек в 3D </param>
        /// <returns> Массив 2D точек (z, x) в проекции на выбранную плоскость </returns>
        public static int[,] XoZ(int[,] data)
        {
            int n = data.GetLength(0);
            int[,] to_return = new int[data.GetLength(0), 2];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    switch (j)
                    {
                        case 0:
                            to_return[i, 0] = data[i, 2];
                            break;
                        case 1:
                            break;
                        case 2:
                            to_return[i, 1] = data[i, 0];
                            break;
                        default:
                            break;
                    }

                }
            }

            return to_return;
        }

        /// <summary>
        /// Проекция на плоскость YoZ
        /// </summary>
        /// <param name="data"> Массив исходных точек в 3D </param>
        /// <returns> Массив 2D точек (y, z) в проекции на выбранную плоскость </returns>
        public static int[,] YoZ(int[,] data)
        {
            int n = data.GetLength(0);
            int[,] to_return = new int[data.GetLength(0), 2];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    switch (j)
                    {
                        case 0:
                            break;
                        case 1:
                            to_return[i, 0] = data[i, 1];
                            break;
                        case 2:
                            to_return[i, 1] = data[i, 2];
                            break;
                        default:
                            break;
                    }

                }
            }

            return to_return;
        }

        /// <summary>
        /// Проверка на выпуклость в какой-то из плоскостей
        /// </summary>
        /// <param name="data"> Координаты точек в какой-то из плоскостей: {x, y}, {z, x}, {y, z} </param>
        /// <returns> Да/нет </returns>
        public static bool Check(int[,] data)
        {
            bool answer = true;
            int N = data.GetLength(0);

            point2D p1;
            point2D p2;
            point2D p3;

            vector2D v1;
            vector2D v2;
            for (int i = 0; i < N - 2; i++)
            {
                p1 = new point2D(data[i, 0], data[i, 1]);
                p2 = new point2D(data[i + 1, 0], data[i + 1, 1]);
                p3 = new point2D(data[i + 2, 0], data[i + 2, 1]);

                v1 = new vector2D(p1, p2);
                v2 = new vector2D(p2, p3);

                if (v1 * v2 <= 0) return false;
            }

            p1 = new point2D(data[N - 2, 0], data[N - 2, 1]);
            p2 = new point2D(data[N - 1, 0], data[N - 1, 1]);
            p3 = new point2D(data[0, 0], data[0, 1]);

            v1 = new vector2D(p1, p2);
            v2 = new vector2D(p2, p3);

            if (v1 * v2 <= 0) return false;

            return true;
        }
    }

    class object3D
    {
        public int e1;
        public int e2;
        public int e3;
    }

    class point3D : object3D
    {
        public point3D()
        {

        }

        public point3D(int X, int Y, int Z)
        {
            e1 = X;
            e2 = Y;
            e3 = Z;
        }

        public point3D(int[] c)
        {
            e1 = c[0];
            e2 = c[1];
            e3 = c[2];
        }

        public int[] Set
        {
            set
            {
                e1 = value[0];
                e2 = value[1];
                e3 = value[2];
            }
        }

        public point3D YoZ
        {
            set => e1 = 0;
            get => new point3D(0, e2, e3);
        }
        public point3D XoZ
        {
            set => e2 = 0;
            get => new point3D(e1, 0, e3);
        }
        public point3D XoY
        {
            set => e3 = 0;
            get => new point3D(e1, e2, 0);
        }
    }

    class vector3D : object3D
    {
        /// Координаты вектора хранятся в e1, e2, e3
        /// public int e1, e2, e3;

        /// Это координаты 2х точек: начало (p1) и конец (p2) вектора
        public point3D p1, p2;

        public vector3D()
        {

        }

        /// <summary>
        /// Конструктор создающий вектор по 2м точкам
        /// </summary>
        /// <param name="_p1"> Начало вектора </param>
        /// <param name="_p2"> Конец вектора </param>
        public vector3D(point3D _p1, point3D _p2)
        {
            p1 = _p1;
            p2 = _p2;
            e1 = p2.e1 - p1.e1;
            e2 = p2.e2 - p1.e2;
            e3 = p2.e3 - p1.e3;
        }

        /// <summary>
        /// Конструктор создающий вектор по координатам 2х точек
        /// </summary>
        /// <param name="c1"> Координаты точки начала </param>
        /// <param name="c2"> Координаты точки конца </param>
        public vector3D(int[] c1, int[] c2)
        {
            p1 = new point3D(c1);
            p2 = new point3D(c2);

            e1 = p2.e1 - p1.e1;
            e2 = p2.e2 - p1.e2;
            e3 = p2.e3 - p1.e3;
        }

        /// <summary>
        /// Конструктор создающий вектор по его координатам
        /// </summary>
        /// <param name="c1"> Координаты вектора </param>
        public vector3D(int[] c1)
        {
            p1 = new point3D();
            p2 = new point3D();

            e1 = c1[0];
            e2 = c1[1];
            e3 = c1[2];
        }

        /// <summary>
        /// Векторное произведение
        /// </summary>
        /// <param name="v1"> Первый вектор </param>
        /// <param name="v2"> Второй вектор </param>
        /// <returns> Результат вычисления - третий вектор </returns>
        public static vector3D operator *(vector3D v1, vector3D v2)
        {
            vector3D v3 = new vector3D();

            v3.p1 = new point3D();
            v3.p2 = new point3D();

            v3.e1 = v1.e2 * v2.e3 - v1.e3 * v2.e2;
            v3.e2 = v1.e3 * v2.e1 - v1.e1 * v2.e3;
            v3.e3 = v1.e1 * v2.e2 - v1.e2 * v2.e1;

            return v3;
        }
    }

    /// <summary>
    /// Далее ненужные фрагменты, вроде как
    /// </summary>

    class object2D
    {
        public int e1;
        public int e2;
    }

    class point2D : object2D
    {
        public point2D()
        {

        }

        public point2D(int X, int Y)
        {
            e1 = X;
            e2 = Y;
        }

        public int[] Set
        {
            set
            {
                e1 = value[0];
                e2 = value[1];
            }
        }
    }

    class vector2D : object2D
    {
        /// Координаты вектора хранятся в e1, e2
        /// public int e1, e2;

        /// Это координаты 2х точек: начало (p1) и конец (p2) вектора
        public point2D p1, p2;

        public vector2D()
        {

        }

        /// <summary>
        /// Конструктор создающий вектор по 2м точкам
        /// </summary>
        /// <param name="_p1"> Начало вектора </param>
        /// <param name="_p2"> Конец вектора </param>
        public vector2D(point2D _p1, point2D _p2)
        {
            p1 = _p1;
            p2 = _p2;
            e1 = p2.e1 - p1.e1;
            e2 = p2.e2 - p1.e2;
        }

        /// <summary>
        /// Псевдовекторное произведение
        /// </summary>
        /// <param name="v1"> Первый вектор </param>
        /// <param name="v2"> Второй вектор </param>
        /// <returns> Результат вычисления: e11*e22 - e12*e21 </returns>
        public static int operator *(vector2D v1, vector2D v2)
        {
            return v1.e1 * v2.e2 - v1.e2 * v2.e1;
        }
    }

}
